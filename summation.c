///////////////////////////////////////////////////////////////////////////////
// University of Hawaii, College of Engineering
// EE 205 - Object Oriented Programming
// Lab 01b - Summation
//
// Usage:  summation n
//   n:  Sum the digits from 1 to n
//
// Result:
//   The sum of the digits from 1 to n
//
// Example:
//   $ summation 6
//   The sum of the digits from 1 to 6 is 21
//
// @author Dane Takenaka <daneht@hawaii.edu>
// @date   1/16/2021
///////////////////////////////////////////////////////////////////////////////

#include <stdio.h>
#include <stdlib.h>

int main(int argc, char *argv[]) {
   int n = atoi(argv[1]);
   int sum;
   int i = 1;
   for (sum = 0; n >= i; i++) {
      sum = sum + i;
   }
   printf("%d\n", sum);
   return 0;
}
